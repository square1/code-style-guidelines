# Version Control

We use Git on all our projects, hosted mostly on BitBucket.

## Repo naming conventions

If the repo contains the source code of a site its name should be the main naked domain name of that site. It should be lowercased.

- Bad: `https://www.square1.io`, `www.square1.io`, `square1.io`
- Good: `square1.io`

Sites that are hosted on a subdomain may use that subdomain in their name

- Bad: `square1.io-guidelines`
- Good: `guidelines.square1.io`

If the repo concerns something else, for example a package, its name should be kebab-cased.

- Bad: `LaravelBackup`, `Spoon`
- Good: `laravel-backup`, `spoon`

## Branches

If you're going to remember one thing in this guide, remember this: **Once a project has gone live, the master branch must always be stable**. It should be safe to deploy the master branch to production at all times. All branches are assumed to be active; stale branches should get cleaned up accordingly.

### Projects in initial development

Projects that aren't live yet have at least two branches: `master` and `develop`. Avoid committing directly on the master branch, always commit through develop.

We use _Feature branches_ with [git-flow](https://danielkummer.github.io/git-flow-cheatsheet/). All the branches should be created from `develop` and not `master`.

### Live projects

Once a project goes live, the `develop` branch gets deleted. All future commits to `master` must be added through a feature branch. In most cases, it's preferred to squash your commits on merge.

There's no strict ruling on feature branch names, just make sure it's clear enough to know what they're for. Branches may only contain lowercase letters and hyphens.

- Bad: `feature/mailchimp`, `random-things`, `develop`
- Good: `feature-mailchimp`, `fix-deliverycosts` or `updates-june-2016`

### Pull requests

Reviewing pull requests is crucial for any project development, the following are some of the criteria we need to take into consideration while reviewing a pull request:

- Code style
- Consistency: avoid introducing new patterns in the code if not needed
- Congruency: every PR should handle only the task at hand and nothing more.

Every pull request should include a description with the following information:

- Description: what was the requirement|issue|error
- Steps to reproduce (include links if needed)
- How was the problem solved
- How to test the solution (include links if needed)
- Link to the task (Jira, ProductiveApp, trello, etc).

Reviewing pull requests is time consuming, so by having all the information available in the pull request description, we can save a lot of time.

Merging branches via pull requests is required.

All pull requests must include at least one reviewer.

Every pull request must be approved by at least one person in the team.

### Merging and rebasing

Ideally, rebase your branch regularly to reduce the chance of merge conflicts.

- If you want to deploy a feature branch to master, use `git merge <branch> --squash`
- If your push is denied, rebase your branch first using `git rebase`

## Commits

There's not strict ruling on commits in projects in initial development, however, descriptive commit messages are recommended. After a project has gone live, descriptive commit messages are required. Always use present tense in commit messages.

- Non-descriptive: `wip`, `commit`, `a lot`, `solid`
- Descriptive: `Update deps`, `Fix vat calculation in delivery costs`

Ideally, prefer granular commits.

- Acceptable: `Cart fixes`
- Better: `Fix add to cart button`, `Fix cart count on home`

## Git Tips

### Creating granular commits with `patch`

If you've made multiple changes but want to split them into more granular commits, use `git add -p`. This will open an interactive session in which you can choose which chunks you want to stage for your commit.

### Moving commits to a new branch

First, create your new branch, then revert the current branch, and finally checkout the new branch.

Don't do this to commits that have already been pushed without double checking with your collaborators!

```bash
git branch my-branch
git reset --hard HEAD~3 # OR git reset --hard <commit>
git checkout my-branch
```

### Squashing commits already pushed

Only execute when you are sure that no-one else pushed changes during your commits.

First, copy the SHA from the commit previous to your commits that need to be squashed.

```bash
git reset --soft <commit>
git commit -m "your new message"
git push --force
```

### Cleaning up local branches

After a while, you'll end up with a few stale branches in your local repository. Branches that don't exist upstream can be cleaned up with `git remote prune origin`. If you want to ensure you're not about to delete something important, add a `--dry-run` flag.

## Resources

- Most of this is based on the [GitHub Flow](https://guides.github.com/introduction/flow/)
- Merge vs. rebase on [Atlassian](https://www.atlassian.com/git/tutorials/merging-vs-rebasing/workflow-walkthrough)
- Merge vs. rebase by [@porteneuve](https://medium.com/@porteneuve/getting-solid-at-git-rebase-vs-merge-4fa1a48c53aa)
- How to Write a Git Commit Message by [@cbeams](https://www.instapaper.com/read/1108354299)
