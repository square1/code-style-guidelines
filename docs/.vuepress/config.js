module.exports = {
  title: "Square1.io",
  description: "Square1.io code-style guidelines.",
  themeConfig: {
    sidebar: [
      ["/laravel-php", "Laravel & PHP"],
      ["/css", "CSS"],
      ["/javascript", "Javascript"],
      ["/version-control", "Version Control"]
    ],
    smoothScroll: true,
    nav: [
      { text: "Home", link: "/" },
      { text: "Square1.io", link: "https://square1.io" }
    ]
  },
  head: [["link", { rel: "icon", href: "https://www.square1.io/favicon.ico" }]]
};
