# Square1 code-style guidelines

A set of rules and guidelines to keep consistency in our projects.

This project uses Vuepress, to learn more visit the [official documentation](https://vuepress.vuejs.org/guide/) website.

## Install

```bash
npm install
```

## Local Development

```bash
npm run docs:dev
```
